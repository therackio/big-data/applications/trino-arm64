ARG PARENT_IMAGE
FROM $PARENT_IMAGE

ARG TRINO_VERSION
ENV TRINO_VERSION=$TRINO_VERSION

LABEL maintainer="gitlab@therack.io"
LABEL description="Trino ${TRINO_VERSION}"
LABEL TRINO_VERSION="${TRINO_VERSION}"

RUN \
    apt update && \
    apt -qq -y install wget python python3 less && \
    apt -qq -y -o Dpkg::Options::="--force-confold" full-upgrade && \
    python --version && \
    python3 --version && \
    mkdir -p /opt && \
    mkdir -p /trino-data && \
    cd /opt && \
    # Get the server
    echo "Geting Server..." && \
    wget https://s3.wasabisys.com/therackio-releases/therackio/big-data/binaries/trino-bin-arm64/$TRINO_VERSION/trino-server-$TRINO_VERSION.tar.gz -q --show-progress --progress=bar:force 2>&1 && \
    mkdir -p /trino-data && \
    tar -xzf /opt/trino-server-$TRINO_VERSION.tar.gz && \
    rm -rf /opt/trino-server-$TRINO_VERSION.tar.gz && \
    ln -s /opt/trino-server-${TRINO_VERSION} /opt/trino && \
    mkdir -p /opt/trino/etc && \
    ls -al /opt/trino && \
    cd / && \
    echo "Getting CLI..."  && \
    # Get the CLI
    wget https://s3.wasabisys.com/therackio-releases/therackio/big-data/binaries/trino-bin-arm64/$TRINO_VERSION/trino-cli-$TRINO_VERSION-executable.jar -q --show-progress --progress=bar:force 2>&1 && \
    mv trino-cli-${TRINO_VERSION}-executable.jar /usr/local/bin/trino && \
    chmod +x /usr/local/bin/trino

ENV TRINO_HOME="/opt/trino"
ENV PATH="${TRINO_HOME}/bin:${PATH}"

RUN \
    # Server Check
    launcher --help && \
    # Client Check
    trino --help

VOLUME ["/trino-data"]

CMD ["bash"]
